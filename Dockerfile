FROM openjdk:latest
MAINTAINER raghu.palakodety@web.de
COPY ./target/assecor-assessment-backend-1.0-SNAPSHOT.jar /usr/app/
WORKDIR /usr/app
RUN sh -c 'touch assecor-assessment-backend-1.0-SNAPSHOT.jar'
ENTRYPOINT ["java", "-jar", "assecor-assessment-backend-1.0-SNAPSHOT.jar"]